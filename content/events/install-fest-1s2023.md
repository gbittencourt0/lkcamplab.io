---
title: "Install Fest 2023"
date: 2023-04-15
draft: false
categories: ["Eventos"]
---

![](/imgs/events/install-fest-2023/install-fest-divulgacao.jpg)

Já teve vontade de instalar Linux no seu computador?

No dia 15/04 (próximo sábado), o LKCAMP realizará um Install Fest para matar essa vontade! O evento ocorrerá na sala 353 do IC-3,5 das 14h às 18h.

E relaxa, você pode instalar o Linux sem tirar o Windows!

Caso queira nos ajudar com as instalações (recebendo um certificado de voluntariado!), é só chegar 30 min mais cedo pra orientações rápidas.

Nos vemos lá! 🐧💻
