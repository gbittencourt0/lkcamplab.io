---
title: "Roda de Conversa com Alex Oliva"
date: 2022-10-21
draft: false
categories: ["Eventos"]
---

![](/imgs/events/bate-papo-oliva/oliva-divulgacao.jpg)

Nessa sexta-feira, Alex Oliva, a maior autoridade de Software Livre do Brasil estará novamente na Unicamp!

Software Livre é o movimento que defende o software ético, transparente, e confiável. Venha conhecer a importância da liberdade de código com a gente!

Esperamos você às 17h no auditório do IC! 🐧
